# The Soulkeeper

The ghosts are hungry for souls, but you, the Grim Reaper, know that their time hasn't come yet. Only you can stop the ghosts from taking the people's souls. 